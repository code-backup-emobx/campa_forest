<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('test');
});
// // Route::get('/', function () {
// //     return view('welcome');
// // });

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::get('statement','FrontendController@statement');
Route::post('statement','FrontendController@add_statement');

Route::get('statement_list','FrontendController@statement_list');
Route::get('edit_statement/{id}','FrontendController@edit_statement');
Route::post('edit_statement/{id}','FrontendController@update_statement');
Route::get('delete_statement/{id}', 'FrontendController@delete_statement');

Route::get('latest_news','FrontendController@latest_news');
Route::post('latest_news','FrontendController@add_latest_news');

Route::get('latest_news_list','FrontendController@latest_news_list');
Route::get('edit_latest_news/{id}','FrontendController@edit_latest_news');
Route::post('edit_latest_news/{id}','FrontendController@update_latest_news');
Route::get('delete_news/{id}', 'FrontendController@delete_news');

Route::get('actions_events_list','FrontendController@actions_events_list');
Route::get('edit_action_event/{id}','FrontendController@edit_action_event');
Route::post('edit_action_event/{id}','FrontendController@update_action_event');
Route::get('delete_event_action/{id}', 'FrontendController@delete_event_action');


Route::get('actions_events','FrontendController@actions_events');
Route::post('actions_events','FrontendController@add_actions_events');

Route::get('about_list','FrontendController@about_list');
Route::get('edit_about/{id}','FrontendController@edit_about');
Route::post('edit_about/{id}','FrontendController@update_about');
Route::get('delete_forest_about/{id}','FrontendController@delete_forest_about');

Route::get('about','FrontendController@about');
Route::post('about','FrontendController@add_about');

Route::get('notification_circulars_orders_list','FrontendController@notification_circulars_orders_list');
Route::get('edit_notification/{id}','FrontendController@edit_notification');
Route::post('edit_notification/{id}','FrontendController@update_notification');
Route::get('delete_notification/{id}','FrontendController@delete_notification');

Route::get('notification_circulars_orders','FrontendController@notification_circulars_orders');
Route::post('notification_circulars_orders','FrontendController@add_notification_oreders');

Route::get('events_list','FrontendController@event_list');
Route::get('edit_event_list/{id}','FrontendController@edit_event_list');
Route::post('edit_event_list/{id}','FrontendController@update_event');
Route::get('delete_event/{id}','FrontendController@delete_event');

Route::get('events','FrontendController@events');
Route::post('events','FrontendController@add_event');

Route::get('important_contacts_list','FrontendController@important_contacts_list');
Route::get('important_contacts','FrontendController@important_contacts');
Route::post('important_contacts','FrontendController@add_important_contacts');
Route::get('delete_contact/{id}','FrontendController@delete_contact');

Route::get('edit_contact/{id}','FrontendController@edit_contact');
Route::post('edit_contact/{id}','FrontendController@update_contact');

Route::get('annual_operation','FrontendController@annual_operation');
Route::post('annual_operation','FrontendController@add_annual_operation');

Route::get('support_list','FrontendController@support_list');
Route::get('add_support','FrontendController@support');
Route::post('add_support','FrontendController@add_support');
Route::get('edit_support/{id}','FrontendController@edit_support');
Route::post('edit_support/{id}','FrontendController@update_support');
Route::get('delete_support/{id}','FrontendController@delete_support');

Route::get('annual_operation_list','FrontendController@annual_operation_list');
Route::get('edit_annual_operation/{id}','FrontendController@edit_annual_operation');
Route::post('edit_annual_operation/{id}','FrontendController@update_annual_operation');
Route::get('delete_operation/{id}','FrontendController@delete_operation');


Route::get('activity_under_pscampa','FrontendController@activity_under_pscampa');
Route::post('activity_under_pscampa','FrontendController@add_activity_under_pscampa');
Route::get('activity_under_pscampa_list','FrontendController@activity_under_pscampa_list');

Route::get('delete_activity_pscampa/{id}','FrontendController@delete_activity_pscampa');

