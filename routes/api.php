<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('forest_statement','ApiController@forest_statement');
Route::get('forest_latest_news','ApiController@forest_latest_news');
Route::get('forest_event_action','ApiController@forest_event_action');
Route::get('forest_notification','ApiController@forest_notification');
Route::get('forest_event','ApiController@forest_event');
Route::get('forest_important_contact', 'ApiController@forest_important_contact');
Route::get('forest_support','ApiController@forest_support');
Route::get('forest_about','ApiController@forest_about');
Route::get('forest_annual_operation','ApiController@forest_annual_operation');
Route::get('activity_category_campa','ApiController@activity_category_campa');
Route::post('activity_subcategory','ApiController@activity_subcategory');
Route::get('activity_under_pscampa','ApiController@activity_under_pscampa');
Route::get('activity_pscampa','ApiController@activity_pscampa');
Route::get('activity_sub_pscampa','ApiController@activity_sub_pscampa');



