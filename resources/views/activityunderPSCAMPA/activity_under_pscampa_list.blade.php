@extends('layouts.master')
@section('content')

    
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            @if(Session::has('success'))

                                    <div class="alert alert-success">

                                        {{ Session::get('success') }}

                                            @php

                                            Session::forget('success');

                                            @endphp

                                    </div>

                                    @endif
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Activity Under PSCAMPA</span>
                            </div>
                            <div class="col-lg-8"></div>
                            <div class="col-lg-1 action">
                                  <a class="btn btn-outline btn-circle btn-sm" href="/activity_under_pscampa"> <i class="fa fa-plus" aria-hidden="true"></i>Add Activity
                                </a>                 
                            </div>
                                    
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                          <div class="table-scrollable">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th> Id </th>
                                                        <th> Annual Plan Heading </th>
                                                        <th> Activity Subcategory Name</th>
                                                        <th> Action </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                 
                                            @foreach($campa_listing  as $listing)
                                            <tr>
                                                <td> {{ $listing->category['category']}} </td>
                                                <td> {{$listing->activity_name}}  </td>
                                                <td> {{$listing->activity_subcategory_name}}  </td>
                                                <td><a href="  " class= "btn btn-success btn-sm"><i class="fa fa-edit"></i>Edit</a>
                                                <a href="{{ url('delete_activity_pscampa/'.$listing->id) }}" class= "btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete this?')" ><i class="fa fa-trash"></i>Delete</a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        
                                        </tbody>
                                    </table>
                                
                                    </div>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

