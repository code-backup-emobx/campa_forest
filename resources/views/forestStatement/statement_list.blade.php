@extends('layouts.master')
@section('content')

    
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            @if(Session::has('success'))

                                    <div class="alert alert-success">

                                        {{ Session::get('success') }}

                                            @php

                                            Session::forget('success');

                                            @endphp

                                    </div>

                                    @endif
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Forest Statement</span>
                            </div>
                            <div class="col-lg-8"></div>
                            <div class="col-lg-1 action">
                                <a class="btn btn-outline btn-circle btn-sm" href="/statement"> <i class="fa fa-plus" aria-hidden="true"></i>Add Statement
                                </a>                  
                            </div>
                           <!--  <a onclick="history.go(-1)" class="btn btn-success">Beat List</a>
                           -->             
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div class="table-scrollable">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th> Id </th>
                                            <th> Description </th>
                                            <th> Profile </th>
                                            <th> Action </th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach($statement_list as $statement_listing)
                                            <tr>
                                                <td> {{$statement_listing->id}} </td>
                                                <td> {{$statement_listing->description}}</td>
                                                <td> <img height="100px" width="100px" src="{{ asset($statement_listing->media) }}"></td>
                                                <td><a href="{{ url('edit_statement/'.$statement_listing->id) }}" class= "btn btn-success btn-sm"><i class="fa fa-edit"></i>Edit</a></td>
                                                <td><a href="{{ url('delete_statement/'.$statement_listing->id) }}" class= "btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete this?')" ><i class="fa fa-trash"></i>Delete</a></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                
                                    </div>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

