@extends('layouts.master')
@section('content')

    
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                           
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Forest Statement</span>
                            </div>
                            
                           <!--  <a onclick="history.go(-1)" class="btn btn-success">Beat List</a>
                           -->             
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <form action="{{ url('/statement') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Statement Text: </label>
                                        <div class="col-lg-6">
                                            <!-- <input type="text" class="form-control" placeholder="Enter text" name="description" > -->
                                            <textarea class="form-control" name="description" placeholder="Enter Description "></textarea>
                                        </div>
                                       
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Upload Picture: </label>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" name="media">
                                               
                                        </div>
                                    </div>
                                   
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                          
                                           <input type="submit" class="btn btn-success" value="Submit">
                                           
                                             <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

