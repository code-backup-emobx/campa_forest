@extends('layouts.master')
@section('content')

    
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            @if(Session::has('success'))

                                    <div class="alert alert-success">

                                        {{ Session::get('success') }}

                                            @php

                                            Session::forget('success');

                                            @endphp

                                    </div>

                                    @endif
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Forest About</span>
                            </div>
                            <div class="col-lg-8"></div>
                            <div class="col-lg-1 action">
                                  <a class="btn btn-outline btn-circle btn-sm" href="/about"> <i class="fa fa-plus" aria-hidden="true"></i>Add Forest About
                                </a>                 
                            </div>
                                    
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                          <div class="table-scrollable">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th> Id </th>
                                                        <th> About Text </th>
                                                        <th> Action </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                         
                                           
                                           @foreach($about_list as $about_listing)
                                            <tr>
                                                <td> {{ $about_listing->id}} </td>
                                                <td>  {{$about_listing->about_text }} </td>
                                                <td><a href="{{ url('edit_about/'.$about_listing->id) }}" class= "btn btn-success btn-sm"><i class="fa fa-edit"></i>Edit</a>
                                                <a href="{{ url('delete_forest_about/'.$about_listing->id) }}" class= "btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete this?')" ><i class="fa fa-trash"></i>Delete</a></td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                
                                    </div>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

