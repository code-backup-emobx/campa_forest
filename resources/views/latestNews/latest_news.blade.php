  @extends('layouts.master')
  @section('content')

    <div style="padding-top: 2%"></div>
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                        <div class="portlet-title">
                            @if(Session::has('success'))
                                <div class="alert alert-success">
                                    {{ Session::get('success') }}
                                        @php
                                        Session::forget('success');
                                        @endphp
                                </div>
                             @endif
                            <div class="caption">
                                <!-- <i class="fa fa-comments" style="color:#36c6d3; size:20px;"></i> -->
                                <span class="caption-subject">Latest News</span>
                            </div>
                            <div class="col-lg-8"></div>
                            <div class="col-lg-1 action">
                                               
                            </div>
                           <!--  <a onclick="history.go(-1)" class="btn btn-success">Beat List</a>
                           -->             
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <form action="{{ url('/latest_news') }}" class="form-horizontal" method="post">
                                  {{ csrf_field() }}
                                <div class="form-body" style="padding-left: 5%">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Latest News Heading: </label>
                                        <div class="col-lg-6">
                                            <!-- <textarea class="ckeditor form-control" name="description" rows="6" data-error-container="#editor2_error"></textarea> -->
                                                <input type="text" class="form-control" placeholder="Enter News Heading" name="news_heading" >
                                        </div>
                                       
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Latest News Date: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" placeholder="Enter Latest Date" name="news_date">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Latest News Description: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" placeholder="Enter Latest Description" name="news_description">
                                        </div>
                                    </div>
                                   
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-lg-3"></div>
                                         <div class="col-lg-6">
                                            <input type="submit" class="btn btn-success" value="Submit">
                                            <a onclick="history.go(-1)" class="btn">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
           
@endsection

