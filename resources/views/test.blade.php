@extends('layouts.master')
@section('content')
        
 <div style="padding-top: 2%"></div>
        <div class="page-content-wrapper">
            <div class="page-content">
                
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->
                        <div class="portlet light portlet-fit bordered">
                                    <div class="portlet-title">
                                         @if(Session::has('success'))

                                        <div class="alert alert-success">

                                            {{ Session::get('success') }}

                                            @php

                                            Session::forget('success');

                                            @endphp

                                        </div>

                                        @endif
                                        
                                        <div class="caption">
                                            <!-- <i class="icon-bubble font-dark"></i> -->
                                            <!-- <span class="caption-subject font-dark bold">Complaint list</span> -->
                                        </div>
                                       <!--  <div class="col-md-10">
                                       
                                          
                                    </div> -->
                                  </div>
                                    <div class="portlet-body">
                                        <div class="table-scrollable">

                                        </div>
                                     </div>
                                </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
       
@endsection

