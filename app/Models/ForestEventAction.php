<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ForestEventAction extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'forest_event_action';
}