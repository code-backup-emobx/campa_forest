<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ForestAbout extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'forest_about';
}