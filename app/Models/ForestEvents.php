<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ForestEvents extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'forest_events';
}