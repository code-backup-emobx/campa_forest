<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ForestImportantContacts extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'forest_important_contacts';
}