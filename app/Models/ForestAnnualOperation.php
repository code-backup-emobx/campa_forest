<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ForestAnnualOperation extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'forest_annual_plan_operations';
}