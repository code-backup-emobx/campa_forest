<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ForestSupport extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'forest_support';
}