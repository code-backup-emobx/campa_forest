<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ForestActivitySubcategory extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'forest_activity_under_punjab_state_subcategory';
    public $timestamps = false;

    public function category(){
        
        return $this->belongsTo('App\Models\ForestActivityCategory', 'category_id');
    } 

    public function sub_cat_detail(){

    	return $this->hasManyThrough('App\Models\Forestsubcat');
    }
}