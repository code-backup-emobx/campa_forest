<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ForestLatestNews extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'forest_latest_news';
}