<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Forestsubcat extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'forest_activity_subcategory';
}