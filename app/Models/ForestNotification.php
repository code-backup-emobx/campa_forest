<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ForestNotification extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'forest_notification';
}