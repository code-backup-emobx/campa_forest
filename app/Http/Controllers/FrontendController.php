<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\ForestStatement;
use App\Models\ForestLatestNews;
use App\Models\ForestEventAction;
use App\Models\ForestNotification;
use App\Models\ForestEvents;
use App\Models\ForestImportantContacts;
use App\Models\ForestSupport;
use App\Models\ForestAbout;
use App\Models\ForestAnnualOperation;
use App\Models\ForestActivityCategory;
use App\Models\ForestActivitySubcategory;

class FrontendController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
   //statement list
    public function statement_list(){

      $statement_list = ForestStatement::all();
      return view('/forestStatement/statement_list',compact('statement_list'));
    }
   //display statement detail for edit
    public function edit_statement( $id ){

      $edit_statement = ForestStatement::where('id',$id)->first();
      return view('/forestStatement/edit_statement',compact('edit_statement'));
    }
   //update statement 
    public function update_statement(Request $request , $id ){

        $description = $request->input('description');
        $media = $request->file('media');
        if($request->hasFile('media') != "") {
          $media = $request->file('media');
          $filename = time() . '.' . $media->getClientOriginalExtension();
          $filename =$media->getClientOriginalName();
          $destinationPath = public_path('/media');
          $media->move($destinationPath, $filename);
          $media = 'media/' . $filename;

          $update = ForestStatement::find($id);
          $update->description = $description;
          $update->media = $media;
          $update->save();

        }
          else{
              $media = "";
          }
          // $update_statement=ForestStatement::where('id', $id)->update(['description' => $description, 'media' => $media]);

          return redirect('/statement_list')->with('success','Forest Statement Updated Successfully');
    }

   //add statement form display
    public function statement(){
        return view('/forestStatement/statement');
    }

   //add statement 
    public function add_statement(Request $request){

      $description = $request->input('description');
      $media = $request->file('media');
      if($request->hasFile('media') != "") {
        $media = $request->file('media');
        $filename = time() . '.' . $media->getClientOriginalExtension();
        $filename =$media->getClientOriginalName();
        $destinationPath = public_path('/media');
        $media->move($destinationPath, $filename);
        $media = 'media/' . $filename;

        $add_statement = new ForestStatement();
        $add_statement->description = $description;
        $add_statement->media = $media;
        $add_statement->save();
      } 
      else{
          $media = "";
      }
      // return $add_statement;
      return redirect('/statement_list')->with('success','Forest Statement added successfully');
   }

   //delete statement
    public function delete_statement($id)
    {
        $deletestatement = DB::table('forest_statement')->where('id',$id)->delete();
        //pa($deletepost); die();
        return redirect()->back()->with('success', 'Statement Deleted Successfully');
    }

   //latest news list 
    public function latest_news_list(){

      $latest_news_list = ForestLatestNews::all();      
      return view('/latestNews/latest_news_list',compact('latest_news_list'));
     }

     //display for edit
     public function edit_latest_news($id){
      $edit_latest_news = ForestLatestNews::where('id',$id)->first();
      return view('/latestNews/edit_latest_news',compact('edit_latest_news'));
     }

     //update latest news
      public function update_latest_news(Request $request, $id ){
          $news_heading = $request->input('news_heading');
          $news_date = $request->input('news_date');
          $news_description = $request->input('news_description');

          $update_statement = ForestLatestNews::find($id);
          $update_statement->news_heading = $news_heading;
          $update_statement->news_date = $news_date;
          $update_statement->news_description = $news_description;
          $update_statement->save();
          return redirect('/latest_news_list')->with('success','Latest News Updated Successfully');

      }

      //view of add latest news
      public function latest_news(){

        return view('/latestNews/latest_news');
      }

      //delete news
      public function delete_news($id)
      {
        $deletelatestnews = DB::table('forest_latest_news')->where('id',$id)->delete();
        //pa($deletepost); die();
        return redirect()->back()->with('success', 'News Deleted Successfully');
      }

      //add latest News
       public function add_latest_news(Request $request){

          $news_heading = $request->input('news_heading');
          $news_date = $request->input('news_date');
          $news_description = $request->input('news_description');

          $add_latest_news = new ForestLatestNews();
          $add_latest_news->news_heading = $news_heading;
          $add_latest_news->news_date = $news_date;
          $add_latest_news->news_description = $news_description;
          $add_latest_news->save();
          return redirect('/latest_news_list')->with('success','Forest Latest News Added Successfully');
        
       }

       //view action and event list
       public function actions_events_list()
       {
          $actions_events_list = ForestEventAction::all();
          return view('/forestActionEvent/actions_events_list',compact('actions_events_list'));
       }
       //edit action event
       public function edit_action_event($id){

          $edit_action_event = ForestEventAction::where('id',$id)->first();
          return view('/forestActionEvent/edit_action_event',compact('edit_action_event'));
       }

       //update action event
       public function update_action_event(Request $request, $id){

        $picture = $request->file('picture');
        if($picture != "") {
            $picture = $picture;
            $filename = time() . '.' . $picture->getClientOriginalExtension();
            $filename =$picture->getClientOriginalName();
            $destinationPath = public_path('/media');
            $picture->move($destinationPath, $filename);
            $picture = '/media/' . $filename;
          } 
          else{
              $media = "";
          }
      $update_statement=ForestEventAction::where('id', $id)->update(['picture' => $picture]);
        return redirect('/actions_events_list')->with('success','Forest Action and Event Updated Successfully');
       }

       //delete action event
      public function delete_event_action($id)
      {
        $deleteventaction = DB::table('forest_event_action')->where('id',$id)->delete();
        //pa($deletepost); die();
        return redirect()->back()->with('success', 'News Deleted Successfully');
      }

       //view action event ui
       public function actions_events(){

        return view('/forestActionEvent/actions_events');
       }

       public function add_actions_events(Request $request){

       $picture = $request->file('picture');
          if($request->hasFile('picture') != "") {
            $image = $request->file('picture');
            $filename =$image->getClientOriginalName();
            $destinationPath = public_path('/media');
            $image->move($destinationPath, $filename);
            $picture = 'media/' . $filename;
          } 
      else {
        $picture = "";
      }

      $add_actions_events = new ForestEventAction();
      $add_actions_events->picture = $picture;
      $add_actions_events->media_type = "image";
      $add_actions_events->save();
      // return $add_actions_events;
      return redirect('/actions_events_list');
   }

   //about_list 
    public function about_list(){

      $about_list = ForestAbout::all();
      return view('/forestAbout/about_list',compact('about_list'));
    }

   //edit about 
    public function edit_about($id){

      $about_edit = ForestAbout::where('id',$id)->first();
      return view('/forestAbout/edit_about',compact('about_edit'));
    }

   //update about
    public function update_about(Request $request,$id){

      $about_text = $request->input('about_text');
      $update_about = ForestAbout::find($id);
      $update_about->about_text = $about_text;
      $update_about->save();

      return redirect('/about_list')->with('success','Forest About Updated Successfully');
    }

    //delete forest about
    public function delete_forest_about($id)
    {
        $deleteforestabout = DB::table('forest_about')->where('id',$id)->delete();
        //pa($deletepost); die();
        return redirect()->back()->with('success', 'Forest About Deleted Successfully');
    }

   //about view
   public function about(){

      return view('/forestAbout/about');
   }

   //Add Forest About
    public function add_about(Request $request){

        $about_text = $request->input('about_text');
        $add_about_text = new ForestAbout();
        $add_about_text->about_text = $about_text;
        $add_about_text->save();
        // return $add_about_text;
        return redirect('/about_list')->with('success','Forest About Added successfully');
    }

   //notification ui
    public function notification_circulars_orders_list(){

        $notification_list = ForestNotification::all();
        return view('/notificationCirculars/notification_circulars_orders_list',compact('notification_list'));
    }

   //edit notification
    public function edit_notification($id){
    
      $edit_notification = ForestNotification::where('id',$id)->first();
      return view('/notificationCirculars/edit_notification',compact('edit_notification'));
    }

   //update notification
   public function update_notification(Request $request , $id){

      $notification_text = $request->input('notification_text');
      $notification_update = ForestNotification::where('id',$id)->update(['notification_text'=>$notification_text]);
      return redirect('/notification_circulars_orders_list')->with('success','Notification Updated Successfully');
   }

  public function notification_circulars_orders(){

      return view('/notificationCirculars/notification_circulars_orders');
    }

    public function add_notification_oreders(Request $request){

        $notification_text = $request->input('notification_text');
        $add_notification = new ForestNotification();
        $add_notification->notification_text = $notification_text;
        $add_notification->save();
        // return $add_notification;
        return redirect('/notification_circulars_orders_list')->with('success','Notification Added Successfully');

    }

    //delete notification
    public function delete_notification($id)
    {
        $forestnotification = DB::table('forest_notification')->where('id',$id)->delete();
        //pa($deletepost); die();
        return redirect()->back()->with('success', 'Forest About Deleted Successfully');
    }

    public function events(){

      return view('/events/events');
    }

     //add event 
    public function add_event(Request $request){

        $event_text = $request->input('event_text');

        $add_event = new ForestEvents();
        $add_event->event_text = $event_text;
        $add_event->save();
        return redirect('/events_list')->with('success','Forest Event Added Successfully');
    }

    public function event_list(){

        $event_list = ForestEvents::all();
        return view('/events/event_list',compact('event_list'));
    }

     //edit event list 
    public function edit_event_list($id){

      $edit_event = ForestEvents::where('id',$id)->first();
      return view('/edit_event',compact('edit_event'));
    }

   //update event 
    public function update_event(Request $request , $id){

        $event_text = $request->input('event_text');

        $update_event = ForestEvents::find($id);
        $update_event->event_text = $event_text;
        $update_event->save();
        return redirect('/events_list')->with('success','Forest Event Updated Successfully');
    }

    //delete notification
    public function delete_event($id)
    {
        $forestevent = DB::table('forest_events')->where('id',$id)->delete();
        //pa($deletepost); die();
        return redirect()->back()->with('success', 'Forest Event Deleted Successfully');
    }

     //important contact list 
    public function important_contacts_list(){

      $contact_list = ForestImportantContacts::all();    
      return view('/importantContact/important_contact_list',compact('contact_list'));
    }

    public function edit_contact($id){

      $edit_contact = ForestImportantContacts::where('id',$id)->first();
      return view('/importantContact/edit_contact',compact('edit_contact'));
    }

    //update contact
    public function update_contact(Request $request , $id){

        $contact_text = $request->input('contact_text');
        $update_contact =  ForestImportantContacts::find($id);
        $update_contact->contact_text = $contact_text;
        $update_contact->save();
        return redirect('/important_contacts_list')->with('success','Forest contact updated Successfully');
    }

    //delete contact
    public function delete_contact($id)
    {
        $forestevent = DB::table('forest_important_contacts')->where('id',$id)->delete();
        //pa($deletepost); die();
        return redirect()->back()->with('success', 'Forest Contact Deleted Successfully');
    }

    public function important_contacts(){

      return view('/importantContact/important_contacts');
    }

    //add important contact
    public function add_important_contacts(Request $request){

      $contact_text = $request->input('contact_text');

      $add_contact = new ForestImportantContacts();
      $add_contact->contact_text = $contact_text;
      $add_contact->save();
      return redirect('/important_contacts_list')->with('success','Forest Important Contact Added Successfully');
    }

    public function annual_operation_list(){

        $operation_list = ForestAnnualOperation::all();
        return view('/annualPlanOperation/annual_plan_operation_list',compact('operation_list'));
    }

    public function annual_operation(){

      return view('/annualPlanOperation/annual_plan_operation');
     }

   public function add_annual_operation(Request $request ){

      $annual_text = $request->input('annual_text');

      $add_annual_operation = new ForestAnnualOperation();
      $add_annual_operation->annual_text = $annual_text;
      $add_annual_operation->save();
      return redirect('/annual_operation_list')->with('success','Annual Operation Added Successfully');
   }

    public function edit_annual_operation($id){

      $edit_annual_operation = ForestAnnualOperation::where('id',$id)->first();
      return view('/annualPlanOperation/edit_annual_operation',compact('edit_annual_operation'));
    }

    public function update_annual_operation(Request $request, $id){

      $annual_text = $request->input('annual_text');
      $add_annual_operation = ForestAnnualOperation::find($id);
      $add_annual_operation->annual_text = $annual_text;
      $add_annual_operation->save();
      return redirect('/annual_operation_list')->with('success','Annual Operation Updated Successfully');
    
    }
    //delete operation
    public function delete_operation($id)
    {
        $forestevent = DB::table('forest_annual_plan_operations')->where('id',$id)->delete();
        //pa($deletepost); die();
        return redirect()->back()->with('success', 'Forest Contact Deleted Successfully');
    }


    public function activity_under_pscampa(){

      $get_category = ForestActivityCategory::all();

      return view('/activityunderPSCAMPA/activity_under_pscampa',['category_listing' => $get_category]);
    }

    public function add_activity_under_pscampa(Request $request ){

      $category_id = $request->category_id;
      $activity_name = $request->input('activity_name');
      
      $add_pscampa = new ForestActivitySubcategory();
      $add_pscampa->category_id = $category_id;
      $add_pscampa->activity_name = $activity_name;
      $add_pscampa->save();
     
      return redirect('/activity_under_pscampa_list')->with('success','Activity Under PSCAMPA Added Successfully');
    }

    public function activity_under_pscampa_list(){
    
    $get_cat_subcat_puncampa = ForestActivitySubcategory::with('category')->get();

      return view('/activityunderPSCAMPA/activity_under_pscampa_list',['campa_listing' => $get_cat_subcat_puncampa]);
    }

    //delete activity pscampa
    public function delete_activity_pscampa($id)
    {
        $forestevent = DB::table('forest_activity_under_punjab_state_subcategory')->where('id',$id)->delete();
        //pa($deletepost); die();
        return redirect()->back()->with('success', 'Forest Contact Deleted Successfully');
    }

    //support
    public function support_list(){

      $support_list = ForestSupport::all();
      return view('/forestSupport/support_list',compact('support_list'));
    }

    //edit support
    public function edit_support( $id ){

     $edit_support = ForestSupport::where('id',$id)->first();
      return view('/forestSupport/edit_support',compact('edit_support'));
    } 
    //update Support
    public function update_support(Request $request , $id){

      $support_text = $request->input('support_text');

      $update_support = ForestSupport::find($id);
      $update_support->support_text = $support_text;
      $update_support->save();
      return redirect('/support_list')->with('success','Forest Support Updated Successfully');
    }

    //delete support
    public function delete_support($id)
    {
        $forestevent = DB::table('forest_support')->where('id',$id)->delete();
        //pa($deletepost); die();
        return redirect()->back()->with('success', 'Forest Support Deleted Successfully');
    }

    public function support(){

      return view('/forestSupport/add_support');
    }

     public function add_support(Request $request){

      $support_text = $request->input('support_text');

      $add_support = new ForestSupport();
      $add_support->support_text = $support_text;
      $add_support->save();
      return redirect('/support_list')->with('success','Forest Support Added Successfully');
    }
 //end class  
}