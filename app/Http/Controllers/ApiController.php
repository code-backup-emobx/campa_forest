<?php

namespace App\Http\Controllers;

use App\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\ForestStatement;
use App\Models\ForestLatestNews;
use App\Models\ForestEventAction;
use App\Models\ForestNotification;
use App\Models\ForestEvents;
use App\Models\ForestImportantContacts;
use App\Models\ForestSupport;
use App\Models\ForestAbout;
use App\Models\ForestAnnualOperation;
use App\Models\ForestActivityCategory;
use App\Models\ForestActivitySubcategory;


class ApiController extends Controller {

    //add statement and picture 
    public function forest_statement(){

        $get_forest_detail = ForestStatement::all();
        $base_url = url('/');
        $forest_statement_result = array();
        $forest_statement_result['error_code'] = "0";
        $forest_statement_result['message'] = 'Forest statement detail';
        $forest_statement_result['Statement_detail'] = $get_forest_detail;
        $forest_statement_result['base_url'] = $base_url;
        return response()->json($forest_statement_result);

    }

    //add forest latest news 
    public function forest_latest_news(){
        
        $get_latest_news = ForestLatestNews::all();
        $forest_latest_news_result = array();
        $forest_latest_news_result['Latest_news_etail'] = $get_latest_news;
        $forest_latest_news_result['error_code'] = "0";
        $forest_latest_news_result['message'] = 'Forest Latest News detail';
        return response()->json($forest_latest_news_result);
    }

    //add forest event actions

    public function forest_event_action(){
      
        $event_and_action = ForestEventAction::all();
        $media_base_url = url('/');
        $forest_actiont_result['Forest_event_and_action_list'] = array();
        $forest_actiont_result['error_code'] = "0";
        $forest_actiont_result['message'] = 'Forest Event And Action List';
        $forest_actiont_result['Forest_event_and_action_list'] = $event_and_action;
        $forest_actiont_result['base_url'] = $media_base_url;
         return response()->json($forest_actiont_result);
    }

    // forest about

    public function forest_about(){

        $add_about_text = ForestAbout::all();
       
        $forest_about_result = array();
        $forest_about_result['Forest_about_list'] = $add_about_text;
        $forest_about_result['error_code'] = "0";
        $forest_about_result['message'] = 'Forest About List';
        return response()->json($forest_about_result);
    }

    //forest notification 

    public function forest_notification(){
        
        $add_notification = ForestNotification::all();
        
        $forest_notification_result = array();
        $forest_notification_result['forest_notification_list'] = $add_notification;
        $forest_notification_result['error_code'] = "0";
        $forest_notification_result['message'] = 'Forest notification list';
        return response()->json($forest_notification_result);
    }

    //forest event text

    public function forest_event(){

        $forest_event = ForestEvents::all();
        $forest_event_result = array();
        $forest_event_result['forest_event_list'] = $forest_event;
        $forest_event_result['error_code'] = "0";
        $forest_event_result['message'] = 'Forest event list';
        return response()->json($forest_event_result);
    }
   
   //forest important contact
    public function forest_important_contact(){
        
        $imortant_contact = ForestImportantContacts::all();
        $forest_important_contact_result = array();
        $forest_important_contact_result['forest_important_contact_list'] = $imortant_contact;
        $forest_important_contact_result['error_code'] = "0";
        $forest_important_contact_result['message'] = 'Forest important contact list';
        return response()->json($forest_important_contact_result);
    }

    //add forest annual plan of operation

    public function forest_annual_operation(){
       
        $forest_annual_operation =  ForestAnnualOperation::all();
        $forest_annual_operstion_result = array();
        $forest_annual_operstion_result['forest_annual_operstion_list'] = $forest_annual_operation;
        $forest_annual_operstion_result['error_code'] = "0";
        $forest_annual_operstion_result['message'] = 'Forest important contact list';
        return response()->json($forest_annual_operstion_result);
       
    }
    //forest support 
    public function forest_support(){

        $support = ForestSupport::all();

        $support_result = array(); 
        $support_result['Support_list'] = $support;
        $support_result['error_code'] = "0";
        $support_result['message'] = "List of Support";
        return response()->json($support_result);
                              
    }

    //activity under punjab state CAMPA

    public function activity_category_campa(){

        $activity_category = ForestActivityCategory::all();

        $result_activity_category = array(); 
        $result_activity_category['activity_category'] = $activity_category;
        $result_activity_category['error_code'] = "0";
        $result_activity_category['message'] = "List of activity list";
        return response()->json($result_activity_category);
    }

    //add category name and subcategory name

    public function activity_subcategory(Request $request){

        $category_id = $request->input('category_id');
        $activity_name = $request->input('activity_name');
        $activity_subcategory_name = $request->input('activity_subcategory_name');

        $add_subcategory = new ForestActivitySubcategory();
        $add_subcategory->category_id = $category_id;
        $add_subcategory->activity_name = $activity_name;
        $add_subcategory->activity_subcategory_name = $activity_subcategory_name;
        $add_subcategory->save();

        $result_subcategory['activity_subcategory'] = array(

            'id' => $add_subcategory->id,
            'category_id' => $add_subcategory->category_id,
            'activity_name' => $add_subcategory->activity_name,
            'activity_subcategory_name' => $add_subcategory->activity_subcategory_name
        ); 

        $result_subcategory['error_code'] = "0";
        $result_subcategory['message'] = "Activity subcategory added successfully";
        return response()->json($result_subcategory);
    }

    public function activity_under_pscampa(){

        $get_plantation_category = ForestActivitySubcategory::where('category_id','1')->get();
        // return $get_plantation_category;die();
        $result_plantation_category['error_code'] = "0";
        $result_plantation_category['message'] = "list of activity under PSCAMPA";
        $result_plantation_category['pscampa_list'] = $get_plantation_category;
        return response()->json($result_plantation_category);
        
    }

    public function activity_pscampa(){

        $get_plantation_subcategory = ForestActivitySubcategory::where('category_id','2')->get();
        // return $get_plantation_category;die();
        $result_plantation_subcategory['error_code'] = "0";
        $result_plantation_subcategory['message'] = "list of activity under PSCAMPA";
        $result_plantation_subcategory['ps_campa_list'] = $get_plantation_subcategory;
        return response()->json($result_plantation_subcategory);
        
    }

    public function activity_sub_pscampa(){

        $get_subcategory = DB::table('forest_activity_under_punjab_state_subcategory')
            ->join('forest_activity_subcategory', 'forest_activity_under_punjab_state_subcategory.id', '=', 'forest_activity_subcategory.id')
            ->get();
        // return $get_subcategory;die();
        $get_plantation_subcategory['error_code'] = "0";
        $get_plantation_subcategory['message'] = "list of activity under PSCAMPA";
        $get_plantation_subcategory['ps_list'] = $get_subcategory;
        return response()->json($get_plantation_subcategory);
    }

//end class   
}

?>